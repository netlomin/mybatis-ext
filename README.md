> 技术讨论: [339763901](https://jq.qq.com/?_wv=1027&k=47urVsH) 

> 项目文档: [Mybatis-Ext文档](http://netlomin.host3v.net)

mybatis-ext解决了Mybatis实体类生成，简单CRUD编写需要机械编码的问题，并能根据规则Mapper方法自动生成查询语句。简化了开发工作，提升了代码可读性. 

**一、扩展功能**

- 支持IDE根据数据表自动生成的JPA实体类.
- 通用CRUD mapper
- 根据规则方法名自动生成查询语句

**二、配置使用**

- 引入maven仓库

```
<repository>
	<id>lm</id>
	<name>lm</name>
	<layout>default</layout>
	<url>http://115.29.4.168:8082/nexus/content/repositories/thirdparty/</url>
</repository>
```

- 引入maven依赖(注意:请删除项目中mybatis, mybatis-spring依赖, 防止版本冲突)

```
<dependency>
    <groupId>org.mybatis</groupId>
	<artifactId>mybatis-ext</artifactId>
	<version>1.0.0</version>
</dependency>
```

- 配置spring

```
<!-- 定义事务 -->
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
	<property name="dataSource" ref="dataSource" />
</bean>
<!-- 配置 Annotation 驱动，扫描@Transactional注解的类定义事务 -->
<tx:annotation-driven transaction-manager="transactionManager" proxy-target-class="true" />
<bean id="sqlSessionFactory" class="org.mybatis.ext.spring.SqlSessionFactoryBean">
	<property name="dataSource" ref="dataSource" />
	<!-- 支持通配 如:org.mybatis.spring.ext.**.entity -->
	<property name="typeAliasesPackage" value="org.mybatis.ext.model.entity" />
	<property name="configLocation" value="classpath:mybatis/mybatis-config.xml"></property>
	<property name="mapperLocations" value="classpath:mybatis/mapper/**/*.xml" />
	<property name="mapperGenerators">
		<list>
			<bean class="org.mybatis.ext.mapper.mysql.CrudMapperGenerator" />
			<bean class="org.mybatis.ext.mapper.mysql.MethodMapperGenerator" />
		</list>
	</property>
</bean>
<!-- 扫描basePackage下所有以@MyBatisDao注解的接口 -->
<bean id="mapperScannerConfigurer" class="org.mybatis.spring.mapper.MapperScannerConfigurer">
	<property name="sqlSessionFactoryBeanName" value="sqlSessionFactory" />
	<!-- 支持通配 如:org.mybatis.spring.ext.**.mapper -->
	<property name="basePackage" value="org.mybatis.ext.model.mapper" />
	<property name="annotationClass" value="org.apache.ibatis.annotations.Mapper" />
</bean>
```

- 配置mybatis

```
<settings>
	<!-- 使全局的映射器启用或禁用缓存。 -->
	<setting name="cacheEnabled" value="true" />
	<!-- 全局启用或禁用延迟加载。当禁用时，所有关联对象都会即时加载。 -->
	<setting name="lazyLoadingEnabled" value="true" />
	<!-- 当启用时，有延迟加载属性的对象在被调用时将会完全加载任意属性。否则，每种属性将会按需要加载。 -->
	<setting name="aggressiveLazyLoading" value="true" />
</settings>
```

**三、开发实例**

- 根据领域模型创建实体表

![实体表](https://static.oschina.net/uploads/img/201703/16225837_oeJ6.png "实体表")

- 通过eclipse生成实体类

![生成实体类](https://static.oschina.net/uploads/img/201703/16231343_KDt5.png "生成实体类")

```
@Entity
@Table(name = "user")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "birth_day")
	private Date birthDay;
	private String name;
	private boolean show;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}
}
```

- 创建Mapper

```
package org.mybatis.spring.ext.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.ext.Page;
import org.mybatis.spring.ext.Pageable;
import org.mybatis.spring.ext.mapper.CrudMapper;
import org.mybatis.spring.ext.mapper.MethodMapper;
import org.mybatis.spring.ext.model.entity.User;

@Mapper
public interface UserMapper extends CrudMapper<User, Long>, MethodMapper<User> {

	/**
	 * 查询 id = #{id} and name = #{name}的用户
	 */
	User getByIdANDName(@Param("id") Long id, @Param("name") String name);

	/**
	 * 查询所有用户
	 */
	List<User> lstAll();

	/**
	 * 查询 name = #{name}的用户的用户数
	 */
	int cntByName(String name);

	/**
	 * 查询 name = #{name}的用户的用户数, name为空时, 查询总用户数
	 */
	int cntBiName(@Param("name") String name);

	/**
	 * 查询 name = #{name}的用户
	 */
	List<User> lstByName(String name);

	/**
	 * 查询 name like #{name}% 的用户
	 */
	List<User> lstByNameStartsWith(Pageable pageable);

	/**
	 * 分页查询:id between #{maxid} and #{minid} and name like %#{name}% order by id desc, birth_day asc limit #{offset}, #{pageSize}
	 */
	Page<User> qryByIdBetweenANDNameContainsOBIdDESCBirthDayASC(Pageable pageable);

	/**
	 * 删除id = #{id}的用户
	 */
	int delById(Long id);
}
```