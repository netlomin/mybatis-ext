package org.mybatis.ext;

/**
 * 常量
 * 
 * @author longmin
 * @version 1.0.0
 */
public interface Const {

	/**
	 * 当主键包含多列时, 需要用&#64;index指明主键列顺序, 此时需要将&#64;index的name设置为该常量,以便标记.
	 * <p>
	 * 例:&#64;Table(name = "user_roles", indexes = &#64;Index(name =
	 * <b>Const.PK</b>, columnList = "userId, roleId"))
	 * </p>
	 */
	String PK = "pk";

	/**
	 * &#64;index属性columnList中列分割符
	 * <p>
	 * 例:&#64;Table(name = "user_roles", indexes =&#64;Index(name = Const.PK,
	 * columnList = "userId<b>,</b> roleId"))
	 * </p>
	 */
	String COLUMN_LIST_SEPARATOR = ",";
}
