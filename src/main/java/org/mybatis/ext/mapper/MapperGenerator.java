package org.mybatis.ext.mapper;

/**
 * Mapper生成器
 * 
 * @author  longmin
 * @version 1.0.0
 */
public interface MapperGenerator {

    /**
     * 检查是否支持
     * 
     * @param  mapperClass
     *                     mapper类
     * @param  methodName
     *                     方法名
     * @return             是否支持生成对应的sqlmap
     */
    boolean supports(Class<?> mapperClass, String methodName);

    /**
     * 生成mapper类method方法对应的mapper文件
     * 
     * @param  mapperClass
     *                     mapper类
     * @param  methodName
     *                     方法名
     * @return             mapper类method方法对应的mapper文件
     */
    String generateMapper(Class<?> mapperClass, String methodName);
}
