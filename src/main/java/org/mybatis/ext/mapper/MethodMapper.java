package org.mybatis.ext.mapper;

/**
 * 方法映射查询
 * 
 * <p>
 * 通过按照一定规则的声明的接口方法(如:getByIdANDNameOBIdASC), 进行查询. 声明规则如下.
 * <ul>
 * <dt>前缀-查询意图</dt>
 * <li>get: 查询并满足条件的<b>一行</b>记录</li>
 * <li>lst: 查询并满足条件的<b>多行</b>记录, 如果参数类型是{@linkplain org.mybatis.ext.bean.Pageable Pageable}, 则只返回指定分页的数据</li>
 * <li>cnt: 查询并满足条件的记录的<b>数目</b></li>
 * <li>del: 删除满足条件的记录</li>
 * <li>qry: 分页查询, 查询参数类型必须为{@linkplain org.mybatis.ext.bean.Pageable Pageable}, 返回值类型必须为{@linkplain org.mybatis.ext.bean.Page Page}</li>
 * </ul>
 * <p>
 * 前缀 与 中缀使用'By', 'Bi'分隔, 当使用By时, 中缀查询条件不检查参数是否为空. 否则只有当参数不为空时, 相应的查询条件才生效.
 * </p>
 * <ul>
 * <dt>中缀-查询条件</dt>
 * <li>查询字段之间使用AND, OR分隔, 字段后可以添加表达式符号(如:getByIdANDNameStartsWith), 无表达式符号的使用'='</li>
 * <li>
 * <ul>
 * <dt>表达式符号说明</dt>
 * <li>Lt : &lt;</li>
 * <li>Le : &lt;=</li>
 * <li>Gt : &gt;</li>
 * <li>Ge : &gt;=</li>
 * <li>Not : &lt;&gt;</li>
 * <li>IsNull : is null</li>
 * <li>NotNull : is not null</li>
 * <li>Between : between minname and maxname(name为字段名)</li>
 * <li>Like : like</li>
 * <li>StartsWith : like name%</li>
 * <li>EndsWith : like %name</li>
 * <li>Contains : like %name%</li>
 * </ul>
 * </li>
 * </ul>
 * <p>
 * 中缀 与 后缀使用'OB'分隔
 * </p>
 * <ul>
 * <dt>后缀-排序方式</dt>
 * <li>排序字段之间使用DESC, ASC分隔(如:qryByNameContainsOBIdDESCNameASC)</li>
 * </ul>
 * </p>
 * 
 * @author  longmin
 * @version 1.0.0
 */
public interface MethodMapper<T> {
}
