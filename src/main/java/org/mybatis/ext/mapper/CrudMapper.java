package org.mybatis.ext.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.ext.bean.Page;

/**
 * 基本CRUD
 * 
 * @param   <T>
 *              实体类泛型
 * @param   <K>
 *              实体主键泛型
 * @author      longmin
 * @version     1.0.0
 */
public interface CrudMapper<T, K> {

    /**
     * 插入数据
     * 
     * <p>
     * 说明: 在插入数据时, 如果不满足自增主键条件(单列主键, 数据库支持并设置了主键自增, id属性上注明了'&#64;GeneratedValue(strategy = GenerationType.AUTO)'), 则必须在插入数据时手动设置主键.
     * </p>
     * 
     * @param  entity
     *                实体对象
     * @return        更新行数
     */
    int insert(T entity);

    /**
     * 批量插入数据
     * 
     * <p>
     * 说明: 在插入数据时, 如果不满足自增主键条件(单列主键, 数据库支持并设置了主键自增, id属性上注明了'&#64;GeneratedValue(strategy = GenerationType.AUTO)'), 则必须在插入数据时手动设置主键.
     * </p>
     * 
     * @param  entitys
     *                 实体对象数组
     * @return         更新行数
     */
    int inserts(@Param("entitys") T... entitys);

    /**
     * 批量插入数据
     * 
     * <p>
     * 说明: 在插入数据时, 如果不满足自增主键条件(单列主键, 数据库支持并设置了主键自增, id属性上注明了'&#64;GeneratedValue(strategy = GenerationType.AUTO)'), 则必须在插入数据时手动设置主键.
     * </p>
     * 
     * @param  entitys
     *                 实体对象集合
     * @return         更新行数
     */
    int inserts(@Param("entitys") Iterable<T> entitys);

    /**
     * 根据<b>主键</b>删除记录
     * 
     * @param  key
     *             主键
     * @return     更新行数
     */
    int delete(K key);

    /**
     * 根据<b>主键</b>修改行
     * 
     * @param  entity
     *                实体对象
     * @return        更新行数
     */
    int update(T entity);

    /**
     * 根据<b>主键</b>查询行(for update)
     * 
     * <p>
     * <b>注意:</b> 如果数据库支持行锁, 该查询会获取行的行锁(其他线程将不能读取该行).
     * </p>
     * 
     * @param  key
     *             主键
     * @return     查询到的行
     */
    T getForUpdate(K key);

    /**
     * 根据<b>主键</b>查询行
     * 
     * @param  key
     *             主键
     * @return     查询到的行
     */
    T get(K key);

    /**
     * 查询所有行
     * 
     * <p>
     * <b>注意:</b> 切勿在大型实体表上调用该方法, 否则会导致严重的系统问题(数据库, 网络阻塞, 服务器内存溢出).
     * </p>
     * 
     * @param  key
     *             主键
     * @return     所有行
     */
    List<T> lstAll();

    /**
     * 查询符合标准的行
     * 
     * <p>
     * 说明: 根据criteria不为空的字段作为条件进行过滤
     * </p>
     * 
     * @param  criteria
     *                  查询标准对象
     * @return          符合标准的行
     */
    List<T> lst(@Param("criteria") T criteria);

    /**
     * 查询符合标准的行
     * 
     * <p>
     * 说明: 根据criteria不为空的字段作为条件进行过滤
     * </p>
     * 
     * @param  criteria
     *                    查询标准对象
     * @param  pageNumber
     *                    查询页
     * @param  pageSize
     *                    查询行数
     * @return            符合标准的行
     */
    List<T> lst(@Param("criteria") T criteria, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize);

    /**
     * 查询符合标准的行
     * 
     * <p>
     * 说明: 根据criteria不为空的字段作为条件进行过滤
     * </p>
     * 
     * @param  criteria
     *                    查询标准对象
     * @param  pageNumber
     *                    查询页
     * @param  pageSize
     *                    查询行数
     * @return            符合标准的行的分页
     */
    Page<T> qry(@Param("criteria") T criteria, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize);
}
