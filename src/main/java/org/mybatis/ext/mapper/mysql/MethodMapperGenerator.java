package org.mybatis.ext.mapper.mysql;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.ext.bean.Pageable;
import org.mybatis.ext.mapper.MethodMapper;
import org.mybatis.ext.utils.AnnotationParser;
import org.mybatis.ext.utils.AnnotationParser.ColumnInfo;
import org.mybatis.ext.utils.BeanUtils;
import org.springframework.util.StringUtils;

public class MethodMapperGenerator extends MysqlMapperGenerator {

	private static final Log LOG = LogFactory.getLog(MethodMapperGenerator.class);
	private static final Pattern REGEXP = Pattern.compile("^(get|lst|qry|cnt|del)(By|Bi)\\w*");
	private static final Pattern TOKENIZER = Pattern.compile("AND|OR|DESC|ASC");
	private static final Pattern SYMBOL = Pattern.compile("Lt|Le|Gt|Ge|Not|IsNull|NotNull|In|NotIn|Between|Like|StartsWith|EndsWith|Contains");
	private static final Pattern PARAMETERS = Pattern.compile("[$#]\\{[^\\}]+\\}");

	@Override
	public boolean supports(Class<?> mapperClass, String methodName) {
		return MethodMapper.class.isAssignableFrom(mapperClass) && REGEXP.matcher(methodName).matches();
	}

	@Override
	public String generateMapper(Class<?> mapperClass, String methodName) {
		Class<?> entityClass = super.getEntityClassGenerics(mapperClass, MethodMapper.class);
		AnnotationParser parser = new AnnotationParser(entityClass);
		String resultType = parser.getEntityName();
		String tableName = super.escape(parser.getTableName());

		StringBuilder xml = new StringBuilder();
		xml.append("<mapper>");
		char c = methodName.charAt(0);
		StringBuilder criteria = this.parseByMethod(methodName, parser);
		switch (c) {
		case 'd':
			xml.append("<delete id=\"").append(methodName).append("\">");
			xml.append("DELETE FROM ").append(tableName).append(criteria);
			xml.append("</delete>");
			break;
		default:
			StringBuilder selectColumns = null;
			if (c == 'q') {
				Map<String, String> parameters = this.parameters(criteria.toString());
				parameters.put("offset", "offset");
				parameters.put("pageSize", "pageSize");
				parameters.put("pageNumber", "pageNumber");
				xml.append("<resultMap id=\"").append(methodName).append("_resultMap\" type=\"Page\">");
				xml.append("<result property=\"pageSize\" column=\"pageSize\"/>");
				xml.append("<result property=\"pageNumber\" column=\"pageNumber\"/>");
				xml.append("<result property=\"totalElements\" column=\"totalElements\"/>");
				xml.append("<collection property=\"content\" select=\"").append(methodName).append("_content\" column=\"").append(parameters)
						.append("\"/>");
				xml.append("</resultMap>");
				selectColumns = super.selectColumns(parser);
				xml.append("<select id=\"").append(methodName).append("_content\" resultType=\"").append(resultType).append("\">");
				xml.append("SELECT ").append(selectColumns).append(" FROM ").append(tableName).append(criteria);
				xml.append(" LIMIT #{offset}, #{pageSize}");
				xml.append("</select>");
				selectColumns = this.pageColumns(parameters);
				int index = criteria.indexOf("ORDER BY");
				if (index != -1) {
					criteria.delete(index - 1, criteria.length());
				}
				resultType = "\" resultMap=\"" + methodName + "_resultMap";
			}
			if (c == 'c') {
				selectColumns = new StringBuilder("count(*)");
				resultType = "\" resultType=\"int";
			} else if (c != 'q') {
				selectColumns = super.selectColumns(parser);
				resultType = "\" resultType=\"" + resultType;
			}
			xml.append("<select id=\"").append(methodName).append(resultType).append("\">");
			xml.append("SELECT ").append(selectColumns).append(" FROM ").append(tableName).append(criteria);
			if (c == 'g') {
				xml.append(" LIMIT 1");
			}
			if (c == 'l') {
				try {
					Method method = BeanUtils.findMethod(mapperClass, methodName, Pageable.class);
					if (method != null) {
						xml.append(" LIMIT #{offset}, #{pageSize}");
					}
				} catch (Exception e) {
				}
			}
			xml.append("</select>");
			break;
		}
		xml.append("</mapper>");
		LOG.info(xml); // 输出生成的sql map 到日志
		return xml.toString();
	}

	private final StringBuilder parseByMethod(String methodName, AnnotationParser parser) {
		StringBuilder sb = new StringBuilder();
		// 魔数: 3: (get|lst|qry|cnt|del) 2:(BY|BI|OB), 5: 3 + 2
		String by = methodName.substring(3, 5);
		boolean useIf = "Bi".equals(by);
		int splitIndex = methodName.lastIndexOf("OB");
		String where = "", orderBy = "";
		if (splitIndex == -1) {
			where = methodName.substring(5);
		} else {
			where = methodName.substring(5, splitIndex);
			orderBy = methodName.substring(splitIndex + 2);
		}
		Map<String, ColumnInfo> fieldMap = parser.getFieldMap();
		// where
		if (where.length() != 0) {
			Matcher matcher = TOKENIZER.matcher(where);
			int offset = 0;
			sb.append(useIf ? " <where>" : " WHERE");
			String operator = "";
			do {
				String expression;
				if (!matcher.find()) {
					expression = where.substring(offset, where.length());
					sb.append(this.parseExpression(operator, expression, fieldMap, useIf));
					break;
				}
				expression = where.substring(offset, matcher.start());
				sb.append(this.parseExpression(operator, expression, fieldMap, useIf));
				operator = matcher.group();
				offset = matcher.end();
			} while (true);
			if (useIf) {
				sb.append("</where>");
			}
		}
		// order by
		if (orderBy.length() != 0) {
			sb.append(" ORDER BY ");
			Matcher matcher = TOKENIZER.matcher(orderBy);
			int offset = 0;
			while (matcher.find()) {
				String field = orderBy.substring(offset, matcher.start());
				String column = fieldMap.get(this.initialToLowerCase(field)).getColumnName();
				column = StringUtils.isEmpty(column) ? super.escape(field) : super.escape(column);
				sb.append(column).append(' ').append(matcher.group()).append(", ");
				offset = matcher.end();
			}
			if (sb.charAt(sb.length() - 2) == ',') {
				sb.delete(sb.length() - 2, sb.length());
			}
		}
		return sb;
	}

	private final StringBuilder parseExpression(String operator, String expression, Map<String, ColumnInfo> fieldMap, boolean useIf) {
		Matcher matcher = SYMBOL.matcher(expression);
		String group = null;
		String field = expression;
		if (matcher.find()) {
			group = matcher.group();
			field = expression.substring(0, matcher.start());
		}
		field = this.initialToLowerCase(field);
		ColumnInfo columnInfo = fieldMap.get(field);
		StringBuilder sb = new StringBuilder();
		if (useIf) {
			sb.append("<if test=\"").append(field).append(" != null");
			Class<?> javaType = columnInfo.getJavaType();
			if (String.class.equals(javaType)) {
				sb.append(" and ").append(field).append(".trim() != ''");
			}
			if (Collection.class.isAssignableFrom(javaType)) {
				sb.append(" and ").append(field).append(".size() != 0");
			}
			if (javaType.isArray()) {
				sb.append(" and ").append(field).append(".length != 0");
			}
			sb.append("\">");
		} else {
			sb.append(' ');
		}
		sb.append(operator);
		if (operator.length() != 0) {
			sb.append(' ');
		}
		String column = columnInfo.getColumnName();
		column = StringUtils.isEmpty(column) ? super.escape(field) : super.escape(column);
		if (group == null) {
			return sb.append(column).append(" = #{").append(field).append(useIf ? "}</if>" : "}");
		} else if ("Lt".equals(group)) {
			return sb.append(column).append(" &lt; #{").append(field).append(useIf ? "}</if>" : "}");
		} else if ("Le".equals(group)) {
			return sb.append(column).append(" &lt;= #{").append(field).append(useIf ? "}</if>" : "}");
		} else if ("Gt".equals(group)) {
			return sb.append(column).append(" &gt; #{").append(field).append(useIf ? "}</if>" : "}");
		} else if ("Ge".equals(group)) {
			return sb.append(column).append(" &gt;= #{").append(field).append(useIf ? "}</if>" : "}");
		} else if ("Not".equals(group)) {
			return sb.append(column).append(" &lt;&gt; #{").append(field).append(useIf ? "}</if>" : "}");
		} else if ("IsNull".equals(group)) {
			return sb.append(column).append(useIf ? " is null</if>" : " is null");
		} else if ("NotNull".equals(group)) {
			return sb.append(column).append(useIf ? " not null</if>" : " not null");
		} else if ("Between".equals(group)) {
			return sb.append(column).append(" BETWEEN #{min").append(field).append("}").append(" AND #{max").append(field)
					.append(useIf ? "}</if>" : "}");
		} else if ("Like".equals(group)) {
			return sb.append(column).append(" LIKE #{").append(field).append(useIf ? "}</if>" : "}");
		} else if ("StartsWith".equals(group)) {
			return sb.append(column).append(" LIKE concat(#{").append(field).append(useIf ? "}, '%')</if>" : "}, '%')");
		} else if ("EndsWith".equals(group)) {
			return sb.append(column).append(" LIKE concat('%', #{").append(field).append(useIf ? "})</if>" : "})");
		} else if ("Contains".equals(group)) {
			return sb.append(column).append(" LIKE concat('%', #{").append(field).append(useIf ? "}, '%')</if>" : "}, '%')");
		} else if ("In".equals(group)) {
			return sb.append(column).append(" IN <foreach collection=\"").append(field).append("s\" item=\"").append(field)
					.append("\" open=\"(\" separator=\",\" close=\")\">#{").append(field).append("}</foreach>");
		} else if ("NotIn".equals(group)) {
			return sb.append(column).append(" NOT IN <foreach collection=\"").append(field).append("s\" item=\"").append(field)
					.append("\" open=\"(\" separator=\",\" close=\")\">#{").append(field).append("}</foreach>");
		}
		return sb;
	}

	private final StringBuilder pageColumns(Map<String, String> column) {
		StringBuilder sb = new StringBuilder("count(*) totalElements");
		for (Entry<String, String> entry : column.entrySet()) {
			sb.append(", #{").append(entry.getKey()).append("} ").append(this.escape(entry.getValue()));
		}
		return sb;
	}

	private final Map<String, String> parameters(String criteria) {
		Matcher matcher = PARAMETERS.matcher(criteria);
		Map<String, String> map = new HashMap<String, String>();
		while (matcher.find()) {
			String field = criteria.substring(matcher.start() + 2, matcher.end() - 1);
			map.put(field, field);
		}
		return map;
	}

	private final String initialToLowerCase(String field) {
		char[] chars = field.toCharArray();
		chars[0] = (char) (chars[0] + 32);
		return new String(chars);
	}
}
