package org.mybatis.ext.spring;

import java.util.List;

import javax.sql.DataSource;

import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.executor.ErrorContext;
import org.apache.ibatis.io.VFS;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.mybatis.ext.ExtConfiguration;
import org.mybatis.ext.mapper.MapperGenerator;
import org.mybatis.spring.transaction.SpringManagedTransactionFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.NestedIOException;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

/**
 * MyBatis SqlSessionFactory 创建工厂
 * 
 * @author longmin
 * @version 1.0.0
 */
public class SqlSessionFactoryBean implements FactoryBean<SqlSessionFactory>, InitializingBean, ApplicationListener<ApplicationEvent> {

    private boolean failFast;
    private SqlSessionFactory sqlSessionFactory;
    private DataSource dataSource;
    private Resource configLocation;
    private Resource[] mapperLocations;
    private String typeAliasesPackage;
    private Class<?> typeAliasesSuperType;
    private Class<? extends VFS> vfs;
    private List<MapperGenerator> mapperGenerators;

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(dataSource, "Property 'dataSource' is required");
        Assert.state(this.configLocation != null && this.configLocation.exists(), "Property 'configLocation' is required");
        // 加载configuration
        ExtConfiguration extConfiguration = new ExtConfiguration(configLocation.getInputStream(), vfs, typeAliasesPackage, typeAliasesSuperType,
                mapperGenerators);
        TransactionFactory transactionFactory = new SpringManagedTransactionFactory();
        extConfiguration.setEnvironment(new Environment(SqlSessionFactoryBean.class.getName(), transactionFactory, this.dataSource));
        // 加载mapper文件
        if (!ObjectUtils.isEmpty(this.mapperLocations)) {
            for (Resource mapperLocation : this.mapperLocations) {
                if (mapperLocation == null) {
                    continue;
                }
                try {
                    XMLMapperBuilder xmlMapperBuilder = new XMLMapperBuilder(mapperLocation.getInputStream(), extConfiguration, mapperLocation.toString(),
                            extConfiguration.getSqlFragments());
                    xmlMapperBuilder.parse();
                } catch (Exception e) {
                    throw new NestedIOException("Failed to parse mapping resource: '" + mapperLocation + "'", e);
                } finally {
                    ErrorContext.instance().reset();
                }
            }
        }
        this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(extConfiguration);
    }

    @Override
    public SqlSessionFactory getObject() throws Exception {
        return this.sqlSessionFactory;
    }

    @Override
    public Class<? extends SqlSessionFactory> getObjectType() {
        return this.sqlSessionFactory == null ? SqlSessionFactory.class : this.sqlSessionFactory.getClass();
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (failFast && event instanceof ContextRefreshedEvent) {
            this.sqlSessionFactory.getConfiguration().getMappedStatementNames();
        }
    }

    /**
     * 如果为true，则对配置进行最终检查，以确保所有映射语句已完全加载，并且没有人仍在等待解析。 默认为false。
     * 
     * @param failFast
     */
    public void setFailFast(boolean failFast) {
        this.failFast = failFast;
    }

    /**
     * 设置数据源
     * 
     * @param dataSource
     */
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Mybatis config 文件路径 <br>
     * 例如: &lt;property&nbsp;name="configLocation"&nbsp;value=
     * "classpath:mybatis/mybatis-config.xml"&nbsp;/&gt;
     * 
     * @param configLocation
     */
    public void setConfigLocation(Resource configLocation) {
        this.configLocation = configLocation;
    }

    /**
     * Mybatis mapper 文件路径, 支持通配<br>
     * 例如: &lt;property&nbsp;name="mapperLocations"&nbsp;value=
     * "classpath:mybatis/mapper/*&#42;/*.xml"&nbsp;/&gt;
     * 
     * @param mapperLocations
     */
    public void setMapperLocations(Resource[] mapperLocations) {
        this.mapperLocations = mapperLocations;
    }

    /**
     * Mybatis 类型别名(TypeAliase)包路径, 支持通配<br>
     * 例如: &lt;property&nbsp;name="typeAliasesPackage"&nbsp;value=
     * "org.mybatis.spring.ext.model.entity"&nbsp;/&gt;
     * 
     * @param typeAliasesPackage
     */
    public void setTypeAliasesPackage(String typeAliasesPackage) {
        this.typeAliasesPackage = typeAliasesPackage;
    }

    /**
     * Mybatis 类型别名(TypeAliase)扫描基类, 如果有配置, 则只扫描其子类, 否则扫描包下的所有类.
     * 
     * @param typeAliasesSuperType
     */
    public void setTypeAliasesSuperType(Class<?> typeAliasesSuperType) {
        this.typeAliasesSuperType = typeAliasesSuperType;
    }

    /**
     * 设置mybatis资源加载类
     * 
     * @param vfs
     */
    public void setVfs(Class<? extends VFS> vfs) {
        this.vfs = vfs;
    }

    /**
     * 配置自定义的SqlMap生成器
     * 
     * @param mapperGenerators
     */
    public void setMapperGenerators(List<MapperGenerator> mapperGenerators) {
        this.mapperGenerators = mapperGenerators;
    }
}