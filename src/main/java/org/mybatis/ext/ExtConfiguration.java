package org.mybatis.ext;

import java.io.InputStream;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.ibatis.executor.ErrorContext;
import org.apache.ibatis.io.VFS;
import org.apache.ibatis.session.Configuration;
import org.mybatis.ext.bean.Page;
import org.mybatis.ext.mapper.MapperGenerator;

/**
 * Mybatis 扩展配置
 * <p>
 * 通过Mybatis配置对象扩展功能
 * </p>
 * 
 * @author longmin
 * @version 1.0.0
 */
public class ExtConfiguration extends Configuration {

    private static final String CONFIG_LOCATION_DELIMITERS = "[,; \t\n]";
    private static final Lock LOCK = new ReentrantLock();

    private List<MapperGenerator> mapperGenerators;

    public ExtConfiguration() {
        super.typeAliasRegistry.registerAlias("Page", Page.class);
    }

    public ExtConfiguration(List<MapperGenerator> mapperGenerators) {
        this();
        this.mapperGenerators = mapperGenerators;
    }

    public ExtConfiguration(InputStream inputStream, Class<? extends VFS> vfs, String typeAliasesPackage, Class<?> typeAliasesSuperType,
            List<MapperGenerator> mapperGenerators) {
        this(mapperGenerators);

        if (vfs != null) {
            this.setVfsImpl(vfs);
        }

        if (typeAliasesPackage != null && typeAliasesPackage.length() != 0) {
            typeAliasesSuperType = typeAliasesSuperType == null ? Object.class : typeAliasesSuperType;
            String[] typeAliasPackages = typeAliasesPackage.split(CONFIG_LOCATION_DELIMITERS);
            for (String typeAliasPackage : typeAliasPackages) {
                super.typeAliasRegistry.registerAliases(typeAliasPackage, typeAliasesSuperType);
            }
        }
        if (inputStream != null) {
            try {
                new ExtConfigBuilder(this, inputStream, null, null).parse();
            } finally {
                ErrorContext.instance().reset();
            }
        }
    }

    /**
     * 当statement不存在时, 尝试使用配置的mapper生成器生成statement.
     */
    @Override
    public boolean hasStatement(String statementName, boolean validateIncompleteStatements) {
        if (super.hasStatement(statementName, false)) {
            return true;
        }
        if (!validateIncompleteStatements) {
            return false;
        }
        this.generateStatement(statementName);
        return super.hasStatement(statementName, validateIncompleteStatements);
    }

    private void generateStatement(String statementName) {
        // 根据statementName获取Mapper接口类型及方法名
        int lastDotIndex = statementName.lastIndexOf('.');
        if (lastDotIndex <= 0) {
            return;
        }
        String mapperClassName = statementName.substring(0, lastDotIndex);
        Class<?> mapperClass;
        try {
            mapperClass = Class.forName(mapperClassName);// 接口类型
        } catch (ClassNotFoundException e) {
            return;
        }
        String methodName = statementName.substring(lastDotIndex + 1);// 方法名
        try {
            LOCK.lock();
            for (MapperGenerator mapperGenerator : this.mapperGenerators) {
                // 相应的statement如果已存在, 则不再调用生成器生成
                if (super.hasStatement(statementName, false)) {
                    break;
                }
                // 检查生成器是否支持
                if (!mapperGenerator.supports(mapperClass, methodName)) {
                    continue;
                }
                // 调用生成器生成statement
                String sqlMap = mapperGenerator.generateMapper(mapperClass, methodName);
                String resource = mapperGenerator.getClass().getName();
                try {
                    new ExtMapperBuilder(sqlMap, this, resource, mapperClassName).parse();
                } finally {
                    ErrorContext.instance().reset();
                }
            }
        } finally {
            LOCK.unlock();
        }
    }

    public List<MapperGenerator> getMapperGenerators() {
        return mapperGenerators;
    }

    public void setMapperGenerators(List<MapperGenerator> mapperGenerators) {
        this.mapperGenerators = mapperGenerators;
    }
}