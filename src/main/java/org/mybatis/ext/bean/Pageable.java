package org.mybatis.ext.bean;

import java.util.HashMap;

import org.mybatis.ext.utils.BeanUtils;

/**
 * 分页参数
 * 
 * <p>
 * 分页查询时, 作为方法的唯一参数
 * </p>
 * 
 * @author longmin
 * @version 1.0.0
 */
public class Pageable extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;

	private static final String pageNumber = "pageNumber";
	private static final String offset = "offset";
	private static final String pageSize = "pageSize";

	/**
	 * 创建一个不包含任何其他参数的分页对象(默认:第0页, 每页10条)
	 */
	public Pageable() {
		this(0, 10);
	}

	/**
	 * 创建一个不包含任何其他参数的分页对象, 其他参数可通过put方法添加.
	 * 
	 * @param page
	 *            查询页
	 * @param size
	 *            分页数目
	 */
	public Pageable(int pageNumber, int pageSize) {
		this.put(Pageable.pageSize, pageSize = (pageSize <= 0 ? 1 : pageSize));
		this.put(Pageable.pageNumber, pageNumber = (pageNumber <= 0 ? 0 : pageNumber));
		this.put(offset, pageNumber * pageSize);
	}

	/**
	 * 创建一个分页对象, 同时将另一个对象的所有属性都映射成分页对象的参数.
	 * 
	 * @param obj
	 *            参数对象
	 * @param page
	 *            查询页
	 * @param size
	 *            分页数目
	 */
	public Pageable(Object obj, int pageNumber, int pageSize) {
		this(pageNumber, pageSize);
		BeanUtils.copyToMap(obj, this);
	}

	/**
	 * 分页开始序号(从0开始计数)
	 */
	public int getOffset() {
		return (Integer) this.get(offset);
	}

	/**
	 * 设置查询页
	 */
	public void setPageNumber(int pageNumber) {
		this.put(Pageable.pageNumber, pageNumber = (pageNumber < 0 ? 0 : pageNumber));
		this.put(offset, pageNumber * getPageSize());
	}

	/**
	 * 设置分页数目
	 */
	public void setPageSize(int pageSize) {
		this.put(Pageable.pageSize, pageSize = (pageSize <= 0 ? 1 : pageSize));
		this.put(offset, getPageNumber() * pageSize);
	}

	/**
	 * 查询页(页码从0开始)
	 */
	public int getPageNumber() {
		return (Integer) this.get(pageNumber);
	}

	/**
	 * 分页数目
	 */
	public int getPageSize() {
		return (Integer) this.get(pageSize);
	}
}
