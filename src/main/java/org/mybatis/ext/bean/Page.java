package org.mybatis.ext.bean;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

/**
 * 分页查询结果
 * <p>
 * <b>注意</b>: 使用分页查询时, 必须在mybatis的配置文件中为该类配置别名:'Page'
 * </p>
 * 
 * @author longmin
 * @version 1.0.0
 */
public class Page<T> implements Iterable<T>, Serializable {
	private static final long serialVersionUID = 1L;

	private int totalElements;
	private int pageSize;
	private int pageNumber;
	private List<T> content;

	/**
	 * 总行数
	 */
	public int getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}

	/**
	 * 分页数目
	 */
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 查询结果数目
	 */
	public int getSize() {
		return content.size();
	}

	/**
	 * 页码(从0计数)
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * 分页结果列表
	 */
	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

	/**
	 * 查询结果数目
	 */
	public int size() {
		return content.size();
	}

	/**
	 * 分页结果迭代器
	 */
	@Override
	public Iterator<T> iterator() {
		return content.iterator();
	}
}
