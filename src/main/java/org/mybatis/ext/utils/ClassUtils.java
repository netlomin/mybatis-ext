package org.mybatis.ext.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 类操作工具
 * 
 * @author longmin
 * @version 1.0.0
 */
public abstract class ClassUtils extends org.springframework.util.ClassUtils {

	private static final Type[] EMPTY_TYPES = new Type[0];

	/**
	 * 获取class类rawType上泛型的实际类型(类 或 类型变量)数组
	 * 
	 * @param clazz
	 *            类
	 * @param rawType
	 *            指定泛型声明类
	 */
	public static Type[] getActualTypeArguments(Class<?> clazz, Class<?> rawType) {
		if (!clazz.isInterface()) {
			// 获取父类上声明的泛型
			Type type = clazz.getGenericSuperclass();
			Type[] types = getActualTypeArguments(type, rawType);
			if (types != null) {
				return types;
			}
			// 递归查找父类的父类
			Class<?> superClass = clazz.getSuperclass();
			if (!Object.class.equals(superClass)) {
				types = getActualTypeArguments(superClass, rawType);
				if (types.length != 0) {
					return types;
				}
			}
		}
		// 查找父接口
		Type[] genericInterfaces = clazz.getGenericInterfaces();
		for (Type type : genericInterfaces) {
			Type[] types = getActualTypeArguments(type, rawType);
			if (types != null) {
				return types;
			}
		}
		// 递归遍历父接口查找
		Class<?>[] interfaces = clazz.getInterfaces();
		for (Class<?> type : interfaces) {
			Type[] types = getActualTypeArguments(type, rawType);
			if (types.length != 0) {
				return types;
			}
		}
		return EMPTY_TYPES;
	}

	/*
	 * 获取类型上的泛型的实际类型
	 * 
	 * @param type 类型
	 * 
	 * @param rawType 指定泛型声明类
	 * 
	 * @return 没有泛型或泛型声明类不等于指定泛型声明类将返回null
	 */
	private static Type[] getActualTypeArguments(Type type, Class<?> rawType) {
		if (!(type instanceof ParameterizedType)) {
			return null;
		}
		ParameterizedType pt = (ParameterizedType) type;
		if (!pt.getRawType().equals(rawType)) {
			return null;
		}
		return pt.getActualTypeArguments();
	}
}
