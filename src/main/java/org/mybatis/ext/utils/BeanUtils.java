package org.mybatis.ext.utils;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * bean工具类
 * 
 * @author longmin
 * @version 1.0.0
 */
public abstract class BeanUtils extends org.springframework.beans.BeanUtils {

	/**
	 * 将对象的属性拷贝到map容器
	 * 
	 * <p>
	 * 说明: 需要拷贝的属性必须包含(非静态)get, set方法, 实际上是拷贝的是get获取的值, 而不是属性本身的值.
	 * </p>
	 * 
	 * @param obj
	 *            对象
	 * @param map
	 *            map
	 */
	public static void copyToMap(final Object obj, final Map<String, Object> map) {
		if (obj == null || map == null) {
			return; // 参数空值, 直接返回
		}
		PropertyIterator iterator;
		try {
			iterator = new PropertyIterator(obj.getClass());
		} catch (IntrospectionException e1) {
			return;
		}
		iterator.iterator(new PropertyVisitor() {
			@Override
			public void visit(Class<?> clazz, String name, Method getter, Method setter) {
				try {
					map.put(name, getter.invoke(obj));
				} catch (Exception e) {// 拷贝属性失败, 直接跳过.
				}
			}
		});
	}

	/**
	 * 属性遍历
	 * 
	 * <p>
	 * 说明: 需要遍历的属性必须包含(公开, 非静态, 参数个数正常)的get, set方法
	 * </p>
	 * 
	 * @author longmin
	 * @version 1.0.0
	 */
	public static class PropertyIterator {

		private final Class<?> clazz;
		private final BeanProperty[] properties;

		public PropertyIterator(Class<?> clazz) throws IntrospectionException {
			BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
			PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
			List<BeanProperty> list = new ArrayList<BeanProperty>();
			for (PropertyDescriptor descriptor : descriptors) {
				Method getter = descriptor.getReadMethod();
				Method setter = descriptor.getWriteMethod();
				if (getter == null || setter == null) {
					continue; // 必须同时具有get, set
				}
				if (getter.getParameterTypes().length != 0 || setter.getParameterTypes().length != 1) {
					continue; // get, set方法的参数长度必须符合要求
				}
				// 优化反射性能
				getter.setAccessible(true);
				setter.setAccessible(true);
				list.add(new BeanProperty(descriptor.getName(), getter, setter));
			}
			this.clazz = clazz;
			this.properties = list.toArray(new BeanProperty[list.size()]);
		}

		/**
		 * 通过访问器遍历属性
		 */
		public void iterator(PropertyVisitor visitor) {
			for (BeanProperty property : properties) {
				visitor.visit(clazz, property.name, property.getter, property.setter);
			}
		}
	}

	static class BeanProperty {
		final String name;
		final Method getter;
		final Method setter;

		public BeanProperty(String name, Method getter, Method setter) {
			this.name = name;
			this.getter = getter;
			this.setter = setter;
		}
	}

	/**
	 * 属性访问器
	 * 
	 * @author longmin
	 * @version 1.0.0
	 */
	public static interface PropertyVisitor {
		/**
		 * 访问属性
		 * 
		 * @param clazz
		 *            类名
		 * @param name
		 *            属性名
		 * @param getter
		 *            get方法
		 * @param setter
		 *            set方法
		 */
		void visit(Class<?> clazz, String name, Method getter, Method setter);
	}
}
