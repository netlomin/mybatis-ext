/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50530
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50530
File Encoding         : 65001

Date: 2017-03-14 18:05:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` varchar(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('id1489478589380', 'name1489478589380', '0');
INSERT INTO `role` VALUES ('id1489478678793', 'name1489478678793', '0');
INSERT INTO `role` VALUES ('id1489479974854', 'name1489479974854', '0');
INSERT INTO `role` VALUES ('id1489480084687', 'name1489480084687', '0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '名字',
  `birth_day` timestamp NULL DEFAULT NULL COMMENT '生日',
  `show` tinyint(255) DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'a', '2017-03-14 14:45:20', '1');
INSERT INTO `user` VALUES ('2', 'b', '2017-03-14 14:46:04', '0');
INSERT INTO `user` VALUES ('13', 'name1489478590046', '2017-03-14 16:03:10', '1');
INSERT INTO `user` VALUES ('14', 'name1489478678982', '2017-03-14 16:04:38', '1');
INSERT INTO `user` VALUES ('15', 'name1489479975081', '2017-03-14 16:26:15', '1');
INSERT INTO `user` VALUES ('16', 'name1489480084847', '2017-03-14 16:28:04', '1');
INSERT INTO `user` VALUES ('17', 'xiaomi', '2017-03-14 17:15:09', '1');
INSERT INTO `user` VALUES ('18', 'xiaomi', '2017-03-14 17:15:09', '1');
INSERT INTO `user` VALUES ('19', 'xiaomi', '2017-03-14 17:15:09', '1');
INSERT INTO `user` VALUES ('20', 'xiaomi', '2017-03-14 17:15:09', '1');
INSERT INTO `user` VALUES ('21', 'xiaomi', '2017-03-14 17:19:55', '1');
INSERT INTO `user` VALUES ('22', 'xiaomi', '2017-03-14 17:19:55', '1');
INSERT INTO `user` VALUES ('23', 'xiaomi', '2017-03-14 17:19:55', '1');
INSERT INTO `user` VALUES ('24', 'xiaomi', '2017-03-14 17:19:55', '1');
INSERT INTO `user` VALUES ('25', 'xiaomi', '2017-03-14 17:21:27', '1');
INSERT INTO `user` VALUES ('26', 'xiaomi', '2017-03-14 17:21:27', '1');
INSERT INTO `user` VALUES ('27', 'xiaomi', '2017-03-14 17:21:27', '1');
INSERT INTO `user` VALUES ('28', 'xiaomi', '2017-03-14 17:21:27', '1');
INSERT INTO `user` VALUES ('29', 'xiaomi', '2017-03-14 17:24:33', '1');
INSERT INTO `user` VALUES ('30', 'xiaomi', '2017-03-14 17:24:33', '1');
INSERT INTO `user` VALUES ('31', 'xiaomi', '2017-03-14 17:24:33', '1');
INSERT INTO `user` VALUES ('32', 'xiaomi', '2017-03-14 17:24:33', '1');
INSERT INTO `user` VALUES ('33', 'xiaomi', '2017-03-14 17:28:10', '1');
INSERT INTO `user` VALUES ('34', 'xiaomi', '2017-03-14 17:28:10', '1');
INSERT INTO `user` VALUES ('35', 'xiaomi', '2017-03-14 17:28:10', '1');
INSERT INTO `user` VALUES ('36', 'xiaomi', '2017-03-14 17:28:10', '1');
INSERT INTO `user` VALUES ('37', 'xiaomi', '2017-03-14 17:30:44', '1');
INSERT INTO `user` VALUES ('38', 'xiaomi', '2017-03-14 17:30:44', '1');
INSERT INTO `user` VALUES ('39', 'xiaomi', '2017-03-14 17:30:44', '1');
INSERT INTO `user` VALUES ('40', 'xiaomi', '2017-03-14 17:30:44', '1');
INSERT INTO `user` VALUES ('41', 'xiaomi', '2017-03-14 17:33:18', '1');
INSERT INTO `user` VALUES ('42', 'xiaomi', '2017-03-14 17:33:18', '1');
INSERT INTO `user` VALUES ('43', 'xiaomi', '2017-03-14 17:33:18', '1');
INSERT INTO `user` VALUES ('44', 'xiaomi', '2017-03-14 17:33:18', '1');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` varchar(25) NOT NULL,
  `enabled` tinyint(255) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('13', 'id1489478589380', '1');
INSERT INTO `user_role` VALUES ('14', 'id1489478678793', '1');
INSERT INTO `user_role` VALUES ('15', 'id1489479974854', '1');
INSERT INTO `user_role` VALUES ('16', 'id1489480084687', '1');
