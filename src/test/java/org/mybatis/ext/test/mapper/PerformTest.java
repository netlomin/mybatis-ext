package org.mybatis.ext.test.mapper;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.ext.test.service.PerformService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context.xml")
public class PerformTest extends AbstractJUnit4SpringContextTests {

	@Resource
	private PerformService performService;

	@Test
	public void testInserts() throws Exception {
		performService.inserts();
	}
}