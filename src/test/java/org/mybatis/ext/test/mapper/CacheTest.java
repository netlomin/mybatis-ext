package org.mybatis.ext.test.mapper;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.ext.model.entity.Role;
import org.mybatis.ext.model.entity.User;
import org.mybatis.ext.model.mapper.RoleMapper;
import org.mybatis.ext.model.mapper.UserMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context.xml")
public class CacheTest extends AbstractJUnit4SpringContextTests {

    @Resource
    private RoleMapper roleMapper; // 无缓存
    @Resource
    private UserMapper userMapper; // 有缓存

    @Test
    public void testNoCache() throws Exception {
        Role insert = new Role();
        insert.setId("id" + System.currentTimeMillis());
        insert.setName("name" + System.currentTimeMillis());
        int count = roleMapper.insert(insert);
        Assert.assertEquals(count, 1);
        Assert.assertTrue(roleMapper.get(insert.getId()) != roleMapper.get(insert.getId()));
    }

    @Test
    public void testCache() throws Exception {
        User insert = new User();
        insert.setName("name" + System.currentTimeMillis());
        int count = userMapper.insert(insert);
        Assert.assertEquals(count, 1);
        User get = userMapper.get(insert.getId());
        Assert.assertTrue(get == userMapper.get(insert.getId()));
        User getByIdANDName = userMapper.getByIdANDName(insert.getId(), insert.getName());
        Assert.assertTrue(getByIdANDName == userMapper.getByIdANDName(insert.getId(), insert.getName()));
        Assert.assertTrue(getByIdANDName != get);
        insert.setName("name" + System.currentTimeMillis());
        userMapper.update(insert);
        Assert.assertTrue(get != userMapper.get(insert.getId()));
        Assert.assertTrue(getByIdANDName != userMapper.getByIdANDName(insert.getId(), insert.getName()));
    }
}
