package org.mybatis.ext.test.mapper.mysql;

import org.junit.Test;
import org.mybatis.ext.mapper.MapperGenerator;
import org.mybatis.ext.mapper.mysql.MethodMapperGenerator;
import org.mybatis.ext.model.mapper.UserMapper;

public class MethodMapperGeneratorTest {

	@Test
	public void testGenerateSqlMap() throws Exception {
		MapperGenerator generator = new MethodMapperGenerator();
		generator.generateMapper(UserMapper.class, "lstBy");
		generator.generateMapper(UserMapper.class, "getByIdANDName");
		generator.generateMapper(UserMapper.class, "getBiIdANDName");
		generator.generateMapper(UserMapper.class, "lstByName");
		generator.generateMapper(UserMapper.class, "cntByName");
		generator.generateMapper(UserMapper.class, "qryByIdBetweenANDNameContainsOBIdDESCBirthDayASC");
		generator.generateMapper(UserMapper.class, "delById");
	}
}
