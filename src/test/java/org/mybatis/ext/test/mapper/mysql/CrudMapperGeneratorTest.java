package org.mybatis.ext.test.mapper.mysql;

import org.junit.Test;
import org.mybatis.ext.mapper.MapperGenerator;
import org.mybatis.ext.mapper.mysql.CrudMapperGenerator;
import org.mybatis.ext.model.mapper.UserRoleMapper;

public class CrudMapperGeneratorTest {

    @Test
    public void testGenerateSqlMap() throws Exception {
        MapperGenerator generator = new CrudMapperGenerator();
        generator.generateMapper(UserRoleMapper.class, "insert");
        generator.generateMapper(UserRoleMapper.class, "update");
        generator.generateMapper(UserRoleMapper.class, "delete");
        generator.generateMapper(UserRoleMapper.class, "getForUpdate");
        generator.generateMapper(UserRoleMapper.class, "get");
        generator.generateMapper(UserRoleMapper.class, "lstAll");
        generator.generateMapper(UserRoleMapper.class, "lst");
        generator.generateMapper(UserRoleMapper.class, "qry");
        generator.generateMapper(UserRoleMapper.class, "inserts");
    }
}
