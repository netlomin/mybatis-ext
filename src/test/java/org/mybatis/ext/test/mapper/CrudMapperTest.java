package org.mybatis.ext.test.mapper;

import java.util.Arrays;
import java.util.Date;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.ext.bean.Page;
import org.mybatis.ext.model.entity.Role;
import org.mybatis.ext.model.entity.User;
import org.mybatis.ext.model.entity.UserRole;
import org.mybatis.ext.model.mapper.RoleMapper;
import org.mybatis.ext.model.mapper.UserMapper;
import org.mybatis.ext.model.mapper.UserRoleMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context.xml")
public class CrudMapperTest extends AbstractJUnit4SpringContextTests {

    @Resource
    private UserMapper userMapper;
    @Resource
    private RoleMapper roleMapper;
    @Resource
    private UserRoleMapper userRolesMapper;

    @Test
    public void testInsert() throws Exception {
        int count;
        Role role = new Role();
        role.setId("id" + System.currentTimeMillis());
        role.setName("name" + System.currentTimeMillis());
        count = roleMapper.insert(role);
        Assert.assertEquals(count, 1);

        User user = new User();
        user.setName("name" + System.currentTimeMillis());
        user.setBirthDay(new Date());
        user.setShow(true);
        count = userMapper.insert(user);
        Assert.assertEquals(count, 1);
        Assert.assertNotNull(user.getId());

        UserRole userRole = new UserRole();
        userRole.setUserId(user.getId());
        userRole.setRoleId(role.getId());
        userRole.setEnabled(true);
        count = userRolesMapper.insert(userRole);
        Assert.assertEquals(count, 1);
    }

    @Test
    public void testInserts() throws Exception {
        int count;
        Role role = new Role();
        role.setId("id" + System.currentTimeMillis());
        role.setName("name" + System.currentTimeMillis());
        count = roleMapper.inserts(role);
        Assert.assertEquals(count, 1);

        User user = new User();
        user.setName("name" + System.currentTimeMillis());
        user.setBirthDay(new Date());
        user.setShow(true);
        count = userMapper.inserts(user, user);
        Assert.assertEquals(count, 2);
        count = userMapper.inserts(Arrays.asList(user, user));
        Assert.assertEquals(count, 2);
    }

    @Test
    public void testUpdate() throws Exception {
        int count;
        Role role = new Role();
        role.setId("id" + System.currentTimeMillis());
        role.setName("name" + System.currentTimeMillis());
        count = roleMapper.update(role);
        Assert.assertEquals(count, 0);

        User user = new User();
        user.setName("name" + System.currentTimeMillis());
        user.setBirthDay(new Date());
        user.setShow(true);
        count = userMapper.update(user);
        Assert.assertEquals(count, 0);

        UserRole userRole = new UserRole();
        userRole.setUserId(user.getId());
        userRole.setRoleId(role.getId());
        userRole.setEnabled(true);
        count = userRolesMapper.update(userRole);
        Assert.assertEquals(count, 0);
    }

    @Test
    public void testDelete() throws Exception {
        int count;
        Role role = new Role();
        role.setId("id" + System.currentTimeMillis());
        role.setName("name" + System.currentTimeMillis());
        count = roleMapper.delete(role.getId());
        Assert.assertEquals(count, 0);

        User user = new User();
        user.setName("name" + System.currentTimeMillis());
        user.setBirthDay(new Date());
        user.setShow(true);
        count = userMapper.delete(user.getId());
        Assert.assertEquals(count, 0);

        UserRole userRole = new UserRole();
        userRole.setUserId(user.getId());
        userRole.setRoleId(role.getId());
        userRole.setEnabled(true);
        count = userRolesMapper.delete(userRole);
        Assert.assertEquals(count, 0);
    }

    @Test
    public void testGet() throws Exception {
        Role role = new Role();
        role.setId("id" + System.currentTimeMillis());
        role.setName("name" + System.currentTimeMillis());
        roleMapper.insert(role);
        User user = new User();
        user.setName("name" + System.currentTimeMillis());
        user.setBirthDay(new Date());
        user.setShow(true);
        userMapper.insert(user);
        UserRole userRole = new UserRole();
        userRole.setUserId(user.getId());
        userRole.setRoleId(role.getId());
        userRole.setEnabled(true);
        userRolesMapper.insert(userRole);

        UserRole get = userRolesMapper.get(userRole);
        Assert.assertEquals(userRole, get);
    }

    @Test
    public void testGetForUpdate() throws Exception {
        Role role = new Role();
        role.setId("id" + System.currentTimeMillis());
        role.setName("name" + System.currentTimeMillis());
        roleMapper.insert(role);
        User user = new User();
        user.setName("name" + System.currentTimeMillis());
        user.setBirthDay(new Date());
        user.setShow(true);
        userMapper.insert(user);
        UserRole userRole = new UserRole();
        userRole.setUserId(user.getId());
        userRole.setRoleId(role.getId());
        userRole.setEnabled(true);
        userRolesMapper.insert(userRole);

        UserRole get = userRolesMapper.getForUpdate(userRole);
        Assert.assertEquals(userRole, get);
    }

    @Test
    public void testLstAll() throws Exception {
        roleMapper.lstAll();
        userMapper.lstAll();
        userRolesMapper.lstAll();
    }

    @Test
    public void testLst() throws Exception {
        Role role = new Role();
        roleMapper.lst(role, 0, 2);
        User user = new User();
        user.setId(1L);
        userMapper.lst(user);
    }

    @Test
    public void testQry() throws Exception {
        Role role = new Role();
        role.setName("name1489479974854");
        Page<Role> rolePage = roleMapper.qry(role, 0, 2);
        System.out.println(rolePage.getTotalElements());
        System.out.println(rolePage.getContent().get(0).getName());
        User user = new User();
        user.setId(1L);
        userMapper.qry(user, 0, 10);
    }
}