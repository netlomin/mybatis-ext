package org.mybatis.ext.test.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.ext.bean.Page;
import org.mybatis.ext.bean.Pageable;
import org.mybatis.ext.model.entity.User;
import org.mybatis.ext.model.mapper.UserMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context.xml")
public class MethodMapperTest extends AbstractJUnit4SpringContextTests {

	@Resource
	private UserMapper userMapper;

	@Test
	public void testMethod() throws Exception {
		List<User> lstAll = userMapper.lstAll();
		List<User> lstBy = userMapper.lstBy();
		Assert.assertEquals(lstBy.size(), lstAll.size());// lstBy

		List<Long> ids = new ArrayList<Long>();
		for (User user : lstAll) {
			ids.add(user.getId());
		}
		int delByIdIn = userMapper.delByIdIn(ids);// 删除所有用户
		Assert.assertEquals(delByIdIn, lstBy.size());
		lstBy = userMapper.lstBy();
		Assert.assertEquals(lstBy.size(), 0);// delByIdIn

		User user = new User();
		user.setName("xiaomi");
		user.setBirthDay(new Date());
		user.setShow(true);
		userMapper.insert(user);
		userMapper.insert(user);
		userMapper.insert(user);
		userMapper.insert(user);
		userMapper.insert(user);
		user.setName("xiaomi+");
		userMapper.insert(user);

		User getByIdANDName = userMapper.getByIdANDName(user.getId(), "xiaomi+");
		Assert.assertEquals(getByIdANDName, user);// getByIdANDName

		int cntByName = userMapper.cntByName(null);
		Assert.assertEquals(cntByName, 0);
		cntByName = userMapper.cntByName("xiaomi");
		Assert.assertEquals(cntByName, 5);// cntByName

		int cntBiName = userMapper.cntBiName("xiaomi");
		Assert.assertEquals(cntBiName, 5);
		cntBiName = userMapper.cntBiName(null);
		Assert.assertEquals(cntBiName, 6);// cntBiName

		List<User> lstByName = userMapper.lstByName("xiaomi");
		Assert.assertEquals(lstByName.size(), cntByName);// lstByName

		Pageable pageable = new Pageable(0, 10);
		pageable.put("name", "xiaomi");
		List<User> lstByNameStartsWith = userMapper.lstByNameStartsWith(pageable);
		Assert.assertEquals(lstByNameStartsWith.size(), 6);
		pageable = new Pageable(0, 5);
		pageable.put("name", "xiaomi");
		lstByNameStartsWith = userMapper.lstByNameStartsWith(pageable);
		Assert.assertEquals(lstByNameStartsWith.size(), 5);// lstByNameStartsWith

		pageable = new Pageable(0, 5);
		pageable.put("name", "xiaomi");
		pageable.put("minid", 0);
		pageable.put("maxid", user.getId());
		Page<User> page = userMapper.qryByIdBetweenANDNameContainsOBIdDESCBirthDayASC(pageable);
		Assert.assertEquals(page.getTotalElements(), 6);
		Assert.assertEquals(page.size(), 5);// qryByIdBetweenANDNameContainsOBIdDESCBirthDayASC

		int delById = userMapper.delById(user.getId());
		Assert.assertEquals(delById, 1);
		user = userMapper.get(user.getId());
		Assert.assertNull(user);// delById
	}
}
