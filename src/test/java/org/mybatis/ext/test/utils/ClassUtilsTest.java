package org.mybatis.ext.test.utils;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

import org.junit.Assert;
import org.junit.Test;
import org.mybatis.ext.utils.ClassUtils;

public class ClassUtilsTest {

	@Test
	public void testGetActualTypeArguments() {
		Type[] types = ClassUtils.getActualTypeArguments(A.class, A.class);
		Assert.assertTrue(types.length == 0);
		types = ClassUtils.getActualTypeArguments(C.class, A.class);
		Assert.assertTrue(types.length == 1 && types[0].equals(C1.class));
		types = ClassUtils.getActualTypeArguments(C.class, B.class);
		Assert.assertTrue(types.length == 1 && types[0] instanceof TypeVariable);
		types = ClassUtils.getActualTypeArguments(D.class, A.class);
		Assert.assertTrue(types.length == 1 && types[0].equals(C1.class));
		types = ClassUtils.getActualTypeArguments(D.class, B.class);
		Assert.assertTrue(types.length == 1 && types[0] instanceof TypeVariable);
		types = ClassUtils.getActualTypeArguments(D.class, C.class);
		Assert.assertTrue(types.length == 1 && types[0].equals(C2.class));
		types = ClassUtils.getActualTypeArguments(F.class, A.class);
		Assert.assertTrue(types.length == 1 && types[0].equals(C1.class));
		types = ClassUtils.getActualTypeArguments(F.class, B.class);
		Assert.assertTrue(types.length == 1 && types[0] instanceof TypeVariable);
		types = ClassUtils.getActualTypeArguments(F.class, C.class);
		Assert.assertTrue(types.length == 1 && types[0].equals(C2.class));
		types = ClassUtils.getActualTypeArguments(F.class, E.class);
		Assert.assertTrue(types.length == 1 && types[0].equals(C1.class));
	}
}

class C1 {
}

class C2 {
}

interface A<T> {
}

interface B<T> {
}

interface C<T> extends A<C1>, B<T> {
}

interface D extends C<C2> {
}

class E<T> {
}

class F extends E<C1> implements C<C2> {
}
