package org.mybatis.ext.test.utils;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mybatis.ext.model.entity.User;
import org.mybatis.ext.utils.BeanUtils;

public class BeanUtilsTest {

	@Test
	public void testCopyToMap() throws Exception {
		User user = new User();
		user.setId(1L);
		user.setName("name");
		user.setShow(true);

		Map<String, Object> map = new HashMap<String, Object>();
		BeanUtils.copyToMap(user, map);

		Assert.assertEquals(map.get("id"), 1L);
		Assert.assertEquals(map.get("name"), "name");
	}
}
