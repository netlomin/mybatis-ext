package org.mybatis.ext.test.service;

import java.util.Date;

import javax.annotation.Resource;

import org.mybatis.ext.model.entity.User;
import org.mybatis.ext.model.mapper.UserMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PerformService {

	@Resource
	private UserMapper userMapper;

	@Transactional(propagation = Propagation.REQUIRED)
	public void inserts() throws Exception {
		User user = new User();
		user.setBirthDay(new Date());
		long start = System.currentTimeMillis();
		for (int i = 0; i < 10000; i++) {
			user.setName("name_" + i);
			userMapper.insert(user);
		}
		System.out.println(System.currentTimeMillis() - start);
	}
}
