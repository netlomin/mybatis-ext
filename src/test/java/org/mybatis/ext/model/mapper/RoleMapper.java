package org.mybatis.ext.model.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.ext.mapper.CrudMapper;
import org.mybatis.ext.model.entity.Role;

@Mapper
public interface RoleMapper extends CrudMapper<Role, String> {
}
