package org.mybatis.ext.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.mybatis.ext.bean.Page;
import org.mybatis.ext.bean.Pageable;
import org.mybatis.ext.mapper.CrudMapper;
import org.mybatis.ext.mapper.MethodMapper;
import org.mybatis.ext.model.entity.User;

@Mapper
@CacheNamespace(readWrite = false)
public interface UserMapper extends CrudMapper<User, Long>, MethodMapper<User> {

	/**
	 * 查询所有用户(相当于CrudMapper.lstAll)
	 */
	List<User> lstBy();

	/**
	 * 批量删除id in #{ids}的用户(返回删除记录数)
	 */
	int delByIdIn(@Param("ids") List<Long> ids);

	/**
	 * 查询 id = #{id} and name = #{name} limit 1 的用户(仅查询一个)
	 */
	User getByIdANDName(@Param("id") Long id, @Param("name") String name);

	/**
	 * 查询 name = #{name} 的用户数量, 当name = null时, 返回结果为0.
	 */
	int cntByName(@Param("name") String name);

	/**
	 * 查询 name = #{name} 的用户数量, 当name = null时, 查询用户总数.
	 */
	int cntBiName(@Param("name") String name);

	/**
	 * 查询 name = #{name}的用户(返回列表), 当name = null时, 返回结果为Empty List.
	 */
	List<User> lstByName(String name);

	/**
	 * 分页查询: name like concat(#{name}, '%') limit #{offset}, #{pageSize}(返回列表),
	 * 当name = null时, 返回结果为Empty List.
	 */
	List<User> lstByNameStartsWith(Pageable pageable);

	/**
	 * 分页查询:id between #{maxid} and #{minid} and name like concat('%', #{name},
	 * '%') order by id desc, birth_day asc limit #{offset}, #{pageSize}(返回分页对象)
	 */
	Page<User> qryByIdBetweenANDNameContainsOBIdDESCBirthDayASC(Pageable pageable);

	/**
	 * 删除id = #{id}的用户(返回删除记录数)(相当于CrudMapper.delete)
	 */
	int delById(Long id);
}
