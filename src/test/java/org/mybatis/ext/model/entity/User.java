package org.mybatis.ext.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import org.mybatis.ext.bean.BaseEntity;

@Table
public class User extends BaseEntity {
	private static final long serialVersionUID = 1L;

	private String name;
	@Column(name = "birth_day")
	private Date birthDay;
	private boolean show;

	public User() {
	}

	public User(Long id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}
}
